// --- this is the babel config which is used when doing the webpack build / devserver.
const config = {
  presets: [
    [
      '@babel/env',
      {
        modules: false,
        // --- remember to import core-js and regenerator runtime at the very beginning of app.js
        useBuiltIns: 'entry',
        // --- yarn add core-js@3
        corejs: 3,
      },
    ],
    '@babel/react',
  ],
  plugins: [
    'styled-components',
    'alleycat-stick-transforms',
  ],
};

module.exports = (api) => {
  api.cache.forever ()
  return config
}
