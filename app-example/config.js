import {
  pipe, compose, composeRight,
  join, take, id, lets,
  tap,
} from 'stick-js/es'

import { fontFace, cssFont, } from 'alleycat-js/es/font'
import { logWith, } from 'alleycat-js/es/general'

import { envIsDev, envIsTst, envIsNotPrd, } from './env'

// const debugRenders = envIsDev
// const debugReducers = envIsDev
// const debugSelectors = envIsDev
const debugRenders = envIsTst
// const debugReducers = envIsTst
const debugReducers = true
const debugSelectors = true

const getMainFontCss = () => join ('\n\n', [
  fontFace (
    'Lora',
    [
      [
        require ('./fonts/lora.woff2'),
        'woff',
      ],
    ],
    {
      weight: 'normal',
      style: 'normal',
      stretch: 'normal',
    },
  ),
])

export default {
  debug: {
    render: debugRenders && {
      // Header: true,
      Main: true,
      // Pain: true,
    },
    reducers: debugReducers && {
      domain: true,
    },
    selectors: debugSelectors && {
      domain: {
        counter: true,
        error: true,
        countedSeven: true,
      },
    },
  },
  font: {
    main: {
      css: getMainFontCss (),
      family: 'Lora',
    },
  },
  images: {
  },
  colors: {
    header: {
      color1: '#EEEEEE',
      color2: '#FFBBBB',
    }
  },
}
