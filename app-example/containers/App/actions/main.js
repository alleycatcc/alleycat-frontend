import {
  pipe, compose, composeRight,
  tap, lets, noop,
} from 'stick-js/es'

import { logWith, } from 'alleycat-js/es/general'
import { action, } from 'alleycat-js/es/redux'

export const localeChange = action (
  (locale) => locale,
)
