import {
  pipe, compose, composeRight,
} from 'stick-js/es'

import { all, call, put, select, takeEvery, takeLatest, delay, } from 'redux-saga/effects'

import configure from 'alleycat-js/es/configure'
import { requestJSON, defaultOpts, resultFoldMap, resultFold, } from 'alleycat-js/es/fetch'
import { EffAction, EffSaga, EffNoEffect, } from 'alleycat-js/es/saga'

import {} from '../actions/main'
import {} from '../store/app/selectors'
import {} from '../store/domain/selectors'

import { doApiCall, saga, toastError, } from '../../../common'
import config from '../../../config'
import {} from '../../../types'

export default function *sagaRoot () {
  yield all ([
    // saga (takeLatest, a_counterIncrement, s_counterIncrement),
  ])
}
