import {
  pipe, compose, composeRight,
  assoc, eq, id,
  condS, guard, otherwise, guardV,
  prop, dot1, tap, drop,
  lets,
  prepend,
  path, ifTrue,
  take, merge, not, split,
  deconstruct, deconstruct2,
} from 'stick-js/es'

import { logWith, composeManyRight, pop, } from 'alleycat-js/es/general'
import { ifPredicateResult, } from 'alleycat-js/es/predicate'
import { makeReducer, } from 'alleycat-js/es/redux'

import { localeChange, } from '../../actions/main'
import { reducer, } from '../../../../common'

export const initialState = {
  // --- `true` means the reducer is totally corrupted and the app should halt.
  error: false,

  locale: 'en',
}

const reducerTable = makeReducer (
  localeChange, (locale) => merge ({ locale, }),
)

export default reducer ('app', initialState, reducerTable)
