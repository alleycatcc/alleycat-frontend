import {
  pipe, compose, composeRight,
} from 'stick-js/es'

import { RequestInit, RequestLoading, RequestError, RequestResults, } from 'alleycat-js/es/fetch'
import { makeReducer, } from 'alleycat-js/es/redux'

import {} from '../../actions/main'

import { reducer, } from '../../../../common'
import {} from '../../../../types'

export const initialState = {
  // --- `error=true` means the reducer is totally corrupted and the app should halt.
  error: false,
}

const reducerTable = makeReducer (
)

export default reducer ('domain', initialState, reducerTable)
