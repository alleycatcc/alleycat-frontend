import {
  pipe, compose, composeRight,
  lets,
} from 'stick-js/es'

import React from 'react'

import { loadable, } from 'alleycat-js/es/react'

import { LoadableLoading, } from '../../alleycat-components/index'

export const Example = lets (
  () => import ('./index'),
  (_loadPromise) => <LoadableLoading/>,
  (loadPromise, fallback) => loadable (
    () => loadPromise, { fallback },
  ),
)
