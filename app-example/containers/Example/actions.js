import {
  pipe, compose, composeRight,
  noop,
} from 'stick-js/es'

import { logWith, } from 'alleycat-js/es/general'
import { action, } from 'alleycat-js/es/redux'

export const counterIncrement = action (
  (stop=false) => stop,
  'counterIncrement',
)

export const osmFetch = action (
  noop, 'osmFetch',
)

export const osmFetchCompleted = action (
  (res) => res,
  'osmFetchCompleted',
)
