import {
  pipe, compose, composeRight,
  split, prop, map, join, assoc,
  ifTrue, lets, noop,
} from 'stick-js/es'

import React, { Fragment, useCallback, useEffect, useRef, useState, } from 'react'

import { FormattedMessage, } from 'react-intl'
import { connect, useDispatch, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'
import styled from 'styled-components'

import configure from 'alleycat-js/es/configure'
import { mapX, iwarn, ierror, logWith, setTimeoutOn, } from 'alleycat-js/es/general'
import { useCallbackConst, } from 'alleycat-js/es/react'
import { useReduxReducer, useSaga, } from 'alleycat-js/es/redux-hooks'
import { media, mediaQuery, } from 'alleycat-js/es/styled'

import { createReducer, } from '../../redux'

import { localeChange, } from '../App/actions/main'
import { selectLocale, } from '../App/store/app/selectors'

import { counterIncrement, osmFetch, } from './actions'
import reducer from './reducer'
import saga from './saga'
import { selectCounter, selectCountedSeven, selectOsm, } from './selectors'

import { Button as ButtonBase, spinner, } from '../../alleycat-components'

import { container, getMessages, useWhy, mediaPhone, mediaTablet, mediaDesktop, mediaTabletWidth, requestResults, } from '../../common'
import config from '../../config'

import {} from '../../types'

const configTop = config | configure.init
const col = configTop.focus ('colors.header')
const { color1, color2, } = col.gets ('color1', 'color2')

const messages = 'app.containers.Example' | getMessages

const Button = (props) => <ButtonBase
  {... props}
  style={{ background: color2, padding: '5px', }}
/>

const ColorsS = styled.div`
  * {
    display: inline-block;
    vertical-align: middle;
    width: 25px;
    height: 25px;
    border: 1px solid black;
  }
  div {
    background: ${color1};
  }
  span {
    background: ${color2};
  }
`

const Colors = () => <ColorsS>
  <div/><span/>
</ColorsS>

const ExampleS = styled.div`
  background: ${color1};
  .x__counter button {
    margin-left: 10px;
    margin-right: 10px;
  }
`

const LangSwitcherS = styled.div`
  margin-left: 10px;
  display: inline-block;
  span.nosel {
    text-decoration: underline;
    cursor: pointer;
  }
`

const LangSwitcher = ({ languages, cur, }) => <LangSwitcherS>
  {languages | mapX ((lang, i) => lets (
    () => cur === lang,
    () => useDispatch (),
    (selected, dispatch) => selected | ifTrue (
      () => ['sel', noop],
      () => ['nosel', () => lang | localeChange | dispatch],
    ),
    () => i === 0 ? '' : ' | ',
    (selected, _, [cls, onClick], sep) => <span key={i}>
      <span>{sep}</span><span onClick={onClick} className={cls}>{lang}</span>
    </span>
  ))}
</LangSwitcherS>

const OsmS = styled.div`
  .x__top {
  }
  .x__bottom {
    border: 1px solid #999999;
    margin-left: 20px;
    width: 60%;
    height: 100px;
    overflow: hidden;
  }
`

const Osm = ({ children, }) => <OsmS>
  {children}
</OsmS>

const Header = () => <>
  <div>
    theme colors
  </div>
  <Colors/>
</>

// --- demonstrates inlining the selectorTable
const Row = container (
  ['Row', {}, { counter: selectCounter, countedSeven: selectCountedSeven, }],
  (props) => {
    const { tick, onClick, counter, countedSeven, } = props
    return <>
      <div>
        tick {tick}
      </div>
      <div className='x__counter'>
        num apples = {counter}
        <Button onClick={onClick}>
          +2
        </Button>
        (got seven? {countedSeven ? 'yes' : 'no'})
      </div>
    </>
  }
)

const dispatchTable = {
  counterIncrementDispatch: counterIncrement,
  osmFetchDispatch: osmFetch,
}

const selectorTable = {
  counter: selectCounter,
  locale: selectLocale,
  osm: selectOsm,
}

const SpinnerComet = spinner ('comet')
const SpinnerS = styled.div`
  margin-left: 20px;
`
const Spinner = (props) => <SpinnerS>
  <SpinnerComet {... props}/>
</SpinnerS>

export default container (
  ['Example', dispatchTable, selectorTable],
  (props) => {
    const {
      isMobile, locale, counter, countedSeven, osm,
      counterIncrementDispatch, osmFetchDispatch,
    } = props

    useReduxReducer ({ createReducer, reducer, key: 'Example', })
    useSaga ({ saga, key: 'Example', })

    const onClick = useCallbackConst (() => counterIncrementDispatch (false))
    const onClickOsm = useCallbackConst (() => osmFetchDispatch ())
    const breakpoint = mediaTabletWidth

    const [tick, setTick] = useState (0)

    useEffect (() => {
      const tickJob = 1000 | setTimeoutOn (() => setTick (tick + 1))
      return () => tickJob | clearTimeout
    })

    useWhy ('Example', props, { tick, })

    return <ExampleS>
      <Header/>
      <div>
        mobile = {isMobile ? 'yes' : 'no'} (page width breakpoint is {breakpoint})
      </div>
      <Osm>
        <div className='x__top'>
          <Button onClick={onClickOsm}>
            Query openstreetmaps
          </Button>
        </div>
        <div className='x__bottom'>
          {osm | requestResults ({ Spinner, onResults: JSON.stringify, })}
        </div>
      </Osm>
      <Row tick={tick} onClick={onClick}/>
      <div>
        <FormattedMessage {...messages.fruit}
          values={{ num: counter, }}
        />
          &nbsp;/&nbsp;
        <FormattedMessage {...messages.mood}
          values={{ num: counter, }}
        />
        <LangSwitcher languages={['nl', 'en']} cur={locale}/>
      </div>
    </ExampleS>
  }
)
