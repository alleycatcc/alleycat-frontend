import {
  pipe, compose, composeRight,
  update, assoc, plus,
} from 'stick-js/es'

import { Nothing, cata, } from 'alleycat-js/es/bilby'
import { RequestInit, RequestLoading, RequestError, RequestResults, } from 'alleycat-js/es/fetch'
import { composeManyRight, } from 'alleycat-js/es/general'
import { makeReducer, } from 'alleycat-js/es/redux'

import { counterIncrement, osmFetch, osmFetchCompleted, } from './actions'
import { reducer, } from '../../common'

export const initialState = {
  counter: 1,
  osm: RequestInit,
}

const reducerTable = makeReducer (
  counterIncrement, () => update ('counter', plus (1)),
  osmFetch, () => assoc ('osm', RequestLoading (Nothing)),
  osmFetchCompleted, (rcomplete) => assoc (
    'osm', rcomplete | cata ({
      RequestCompleteError: (err) => RequestError (err),
      RequestCompleteSuccess: (res) => RequestResults (res),
    }),
  ),
)

export default reducer ('Example', initialState, reducerTable)
