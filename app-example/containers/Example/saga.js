import {
  pipe, compose, composeRight,
} from 'stick-js/es'

import { all, call, delay, put, select, takeEvery, takeLatest, } from 'redux-saga/effects'

import { requestJSON, defaultOpts, resultFoldMap, resultFold, } from 'alleycat-js/es/fetch'
import { EffAction, EffSaga, EffNoEffect, } from 'alleycat-js/es/saga'

import {
  counterIncrement as a_counterIncrement,
  osmFetch as a_osmFetch,
  osmFetchCompleted as a_osmFetchCompleted,
} from './actions'
import {} from './selectors'

import { doApiCall, saga, toastError, } from '../../common'

export function *s_counterIncrement (stop) {
  if (stop) return
  yield a_counterIncrement (true) | put
}

export function *s_osmFetch () {
  yield delay (3000)
  yield call (doApiCall, {
    url: '/api/osm/node/7297660565',
    optsMerge: {
      headers: {
        Accept: 'application/json',
      },
    },
    oops: toastError,
    continuation: EffAction (a_osmFetchCompleted),
    imsgDecorate: 'Error fetching OSM data',
  })
}

export default function *sagaRoot () {
  yield all ([
    saga (takeLatest, a_counterIncrement, s_counterIncrement),
    saga (takeLatest, a_osmFetch, s_osmFetch),
  ])
}
