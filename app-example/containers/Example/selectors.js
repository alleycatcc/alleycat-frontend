import {
  pipe, compose, composeRight,
  gte,
} from 'stick-js/es'

import { createSelector, defaultMemoize as memoize, } from 'reselect'

import {} from 'alleycat-js/es/general'

import { initialState, } from './reducer'

import { initSelectors, } from '../../common'

const { select, selectTop, selectVal, } = initSelectors (
  'Example',
  initialState,
)

export const selectCounter = selectVal ('counter')
export const selectCountedSeven = select (
  'countedSeven',
  [selectCounter],
  7 | gte,
)

export const selectOsm = selectVal ('osm')
