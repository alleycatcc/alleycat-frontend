import {
  pipe, compose, composeRight,
} from 'stick-js/es'

import React, { Fragment, useCallback, useEffect, useRef, useState, } from 'react'

import { IntlProvider, } from 'react-intl'
import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'
import styled from 'styled-components'

import { Nothing, Just, fold, cata, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { stopPropagation, } from 'alleycat-js/es/dom'
import { mapX, iwarn, ierror, logWith, setTimeoutOn, } from 'alleycat-js/es/general'
import { useCallbackConst, } from 'alleycat-js/es/react'
import { media, mediaQuery, } from 'alleycat-js/es/styled'

import {} from '../App/actions/main'
import { selectLocale, } from '../App/store/app/selectors'

import { Header, } from '../../components/Header/Loadable'

import { container, useWhy, mediaPhone, mediaTablet, mediaDesktop, } from '../../common'
import config from '../../config'

import {} from '../../types'

const dispatchTable = {
}

const selectorTable = {
  locale: selectLocale,
}

export default container (
  ['LanguageProvider', dispatchTable, selectorTable],
  (props) => {
    const { children, locale='en', messages, } = props
    return <IntlProvider
      locale={locale}
      key={locale}
      messages={messages [locale]}
    >
      { /* the production build will fail if this is { ... children} */ }
      { React.Children.only (children) }
    </IntlProvider>
  },
)
