import {
  pipe, compose, composeRight,
} from 'stick-js/es'

import React, { Fragment, useCallback, useEffect, useRef, useState, } from 'react'

import { FormattedMessage, } from 'react-intl'
import { connect, useDispatch, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'
import styled from 'styled-components'

import { useParams as useRouteParams, } from 'react-router-dom'

import { Nothing, Just, fold, cata, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { stopPropagation, } from 'alleycat-js/es/dom'
import { mapX, iwarn, ierror, logWith, setTimeoutOn, } from 'alleycat-js/es/general'
import { useCallbackConst, } from 'alleycat-js/es/react'
import { useReduxReducer, useSaga, } from 'alleycat-js/es/redux-hooks'
import { media, mediaQuery, } from 'alleycat-js/es/styled'

import {} from '../App/actions/main'
import { selectLocale, } from '../App/store/app/selectors'

import saga from './saga'

import { spinner, } from '../../alleycat-components'

import { Example, } from '../../containers/Example/Loadable'

import { container, getMessages, useWhy, mediaPhone, mediaTablet, mediaDesktop, mediaTabletWidth, requestResults, } from '../../common'
import config from '../../config'

import {} from '../../types'

const messages = 'app.containers.Main' | getMessages

const MainS = styled.div`
`

const dispatchTable = {
}

const selectorTable = {
  locale: selectLocale,
}

export default container (
  ['Main', dispatchTable, selectorTable],
  (props) => {
    const { isMobile, locale, } = props

    const _params = useRouteParams ('param')

    useWhy ('Main', props)
    useSaga ({ saga, key: 'Main', })

    return <MainS>
      <Example isMobile={isMobile}/>
    </MainS>
  }
)
