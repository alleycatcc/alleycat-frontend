import {
  pipe, compose, composeRight,
  noop, id, always,
  T, F, tap,
  concat, map,
  defaultTo,
  die, concatTo, precatTo,
  recurry,
} from 'stick-js/es'

import daggy from 'daggy'

import { Just, Nothing, cata, fold, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { logWith, iwarn, ierror, toString, } from 'alleycat-js/es/general'
import { ifTrueV, } from 'alleycat-js/es/predicate'

import config from './config'
