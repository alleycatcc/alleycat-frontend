#!/usr/bin/env node
import _defineProperty from "@babel/runtime/helpers/defineProperty";
import _toConsumableArray from "@babel/runtime/helpers/toConsumableArray";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

import { pipe, compose, composeRight, die, sprintf1, sprintfN, recurry, whenOk, tap, ifTrue, id, concatTo, ifOk } from 'stick-js/es';
import fs from 'fs';
import { join as pathJoin } from 'path';
import { flatMap } from 'alleycat-js/es/bilby';
import yargsMod from 'yargs';
import { bindir, brightRed, chdir, coerceDirOk, coerceFileOk, coerceCreateDirIfNotExists, coerceDirMustNotExist, coerceFileMustNotExist, env, error, info, log, realpath, sys1, yellow } from './util.mjs';
var flatten = flatMap(id);
var whenOkList = recurry(2)(function (yes) {
  return ifOk(yes, function () {
    return [];
  });
});
var rootDir = pathJoin(bindir, '../..');
var appExample = pathJoin(rootDir, 'app-example');
var configExample = pathJoin(rootDir, 'config-example.mjs');
var runDir = pathJoin(rootDir, 'app-babel');
var mainlibDir = pathJoin(rootDir, 'main/lib');
var webpackConfigDevserver = pathJoin(mainlibDir, 'webpack/webpack.devserver.babel.mjs');
var webpackConfigBuild = pathJoin(mainlibDir, 'webpack/webpack.build.babel.mjs');
var plopfile = pathJoin(mainlibDir, 'generators/index.mjs');

var cmd = function cmd(argv, next) {
  var doChdir = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

  var whenOkSetEnv = function whenOkSetEnv(key) {
    return whenOk(env(key));
  };

  pipe(argv.appDir, whenOkSetEnv('APP_DIR'));
  pipe(argv.buildDir, whenOkSetEnv('BUILD_DIR'));
  pipe(argv.configFile, whenOkSetEnv('AC_CONFIG_FILE'));
  if (doChdir) chdir(runDir);
  next(argv);
};

var _runDevServer = function _runDevServer() {
  var host = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var port = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  return sys1.apply(void 0, _toConsumableArray(flatten([['npx', 'webpack', 'serve', '--config', webpackConfigDevserver]].concat(_toConsumableArray(pipe(port, whenOkList(concatTo(['--port'])))), _toConsumableArray(pipe(host, whenOkList(concatTo(['--host']))))))));
};

var cmdInit = function cmdInit(argv) {
  sys1('cp', '-ar', appExample, argv.appDir);
  info(pipe(yellow(argv.appDir), sprintf1('created %s')));
  pipe(argv.withExample, ifTrue(function () {
    sys1('cp', '-a', configExample, argv.configFile);
  }, function () {
    var main = pathJoin(argv.appDir, 'containers/Main/index.js');
    fs.writeFileSync(argv.configFile, 'export default {}');
    sys1('rm', '-rf', pathJoin(argv.appDir, 'containers/Example'));
    sys1('sed', '-i', '-e', '/import.\\+containers\\/Example/d;', main);
    sys1('sed', '-i', '-e', '/<Example/d', main);
  }));
  info(pipe(yellow(argv.configFile), sprintf1('created %s')));
};

var cmdDevServer = function cmdDevServer(argv) {
  env('WEBPACK_TYPE', 'dev');
  env('NODE_ENV', 'development');

  _runDevServer(argv.host, argv.port);
};

var cmdDevServerPrd = function cmdDevServerPrd(argv) {
  env('WEBPACK_TYPE', 'dev');
  env('NODE_ENV', 'production');

  _runDevServer();
};

var cmdBuild = function cmdBuild(whichEnv) {
  return function (argv) {
    env('APP_ENV', whichEnv);
    env('NODE_ENV', 'production');
    env('NODE_OPTIONS', '--trace-deprecation');
    env('WEBPACK_TYPE', 'build');
    sys1('npx', 'webpack', '--config', webpackConfigBuild, '--color', '--progress');
  };
};

var generate = function generate(argv) {
  var appDir = realpath(argv.appDir);
  sys1('npx', 'plop', '--plopfile', plopfile, '--dest', appDir);
};

var yargs = yargsMod(process.argv.slice(2)).usage("Usage: node $0 {command} [options]").strict().help('h').alias('h', 'help').showHelpOnFail(false, 'Specify --help for available options').command({
  command: '$0',
  handler: function handler(argv) {
    return yargs.showHelp(error);
  }
});

var addOptsInit = function addOptsInit(yargs) {
  return yargs.option('app-dir', {
    describe: "app dir to create, must not exist, e.g. 'app'",
    demandOption: true,
    coerce: coerceDirMustNotExist
  }).option('config-file', {
    describe: "config file to create, must not exist, e.g. 'config.mjs'",
    demandOption: true,
    coerce: coerceFileMustNotExist
  });
};

var addOptsNonInit = function addOptsNonInit(yargs) {
  return yargs.option('app-dir', {
    describe: "app dir",
    demandOption: true,
    coerce: composeRight(coerceDirOk, realpath)
  }).option('config-file', {
    describe: "config file (e.g. config.mjs)",
    demandOption: true,
    coerce: composeRight(coerceFileOk, realpath)
  });
};

var optsDev = {
  builder: function builder(yargs) {
    return pipe(yargs, addOptsNonInit).option('port', {
      describe: 'port',
      "default": 3000,
      number: true
    }).option('host', {
      describe: 'host'
    });
  }
};
var optsBuild = {
  builder: function builder(yargs) {
    return pipe(yargs, addOptsNonInit).option('build-dir', {
      describe: 'the directory in which to build (will be created if necessary)',
      demandOption: true,
      coerce: composeRight(coerceCreateDirIfNotExists, realpath)
    });
  }
};
yargs.command({
  command: 'init',
  describe: 'initialize your project',
  builder: function builder(yargs) {
    return pipe(yargs, addOptsInit).option('with-example', {
      describe: 'include <Example/> component and API call',
      "boolean": true
    });
  },
  handler: function handler(argv) {
    return cmdInit(argv);
  }
}).command(_objectSpread({
  command: 'devserver',
  describe: 'run the dev server',
  handler: function handler(argv) {
    return cmd(argv, cmdDevServer);
  }
}, optsDev)).command(_objectSpread({
  command: 'devserver-prd',
  describe: 'run the dev server in production mode',
  handler: function handler(argv) {
    return cmd(argv, cmdDevServerPrd);
  }
}, optsDev)).command(_objectSpread({
  command: 'build-tst',
  describe: 'build the app in testing mode',
  handler: function handler(argv) {
    return cmd(argv, cmdBuild('tst'));
  }
}, optsBuild)).command(_objectSpread({
  command: 'build-acc',
  describe: 'build the app in acceptance mode',
  handler: function handler(argv) {
    return cmd(argv, cmdBuild('acc'));
  }
}, optsBuild)).command(_objectSpread({
  command: 'build-prd',
  describe: 'build the app in production mode',
  handler: function handler(argv) {
    return cmd(argv, cmdBuild('prd'));
  }
}, optsBuild)).command(_objectSpread({
  command: 'build-prd-profile',
  describe: 'build the app in production mode with profiling',
  handler: function handler(argv) {
    return cmd(argv, cmdBuild('prd-profile'));
  }
}, optsBuild)).command({
  command: 'generate',
  describe: 'add new components/containers',
  handler: function handler(argv) {
    return cmd(argv, generate);
  },
  builder: function builder(yargs) {
    return pipe(yargs, addOptsNonInit);
  }
}).argv;