import { pipe, compose, composeRight, sprintfN, recurry, tap, whenTrue, die } from 'stick-js/es';
import fs from 'fs';
import { dirname } from 'path';
import { fileURLToPath } from 'url';
import fishLib from 'fish-lib';

var _fishLib$forceColors = fishLib.forceColors(),
    log = _fishLib$forceColors.log,
    info = _fishLib$forceColors.info,
    warn = _fishLib$forceColors.warn,
    error = _fishLib$forceColors.error,
    green = _fishLib$forceColors.green,
    yellow = _fishLib$forceColors.yellow,
    magenta = _fishLib$forceColors.magenta,
    brightRed = _fishLib$forceColors.brightRed,
    cyan = _fishLib$forceColors.cyan,
    brightBlue = _fishLib$forceColors.brightBlue,
    sprintf = _fishLib$forceColors.sprintf,
    getopt = _fishLib$forceColors.getopt,
    bulletSet = _fishLib$forceColors.bulletSet,
    sysSpawn = _fishLib$forceColors.sysSpawn;

export { log, info, warn, error, green, yellow, magenta, brightRed, cyan, brightBlue, sprintf, getopt, bulletSet, sysSpawn };
bulletSet({
  type: 'star'
});
export var bindir = dirname(fileURLToPath(import.meta.url));
export var sys1 = function sys1(prog) {
  for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    args[_key - 1] = arguments[_key];
  }

  return sysSpawn(prog, args, {
    die: true,
    sync: true,
    verbose: true,
    outPrint: true
  });
};
export var chdir = function chdir(dir) {
  info(pipe([yellow('chdir'), dir], sprintfN('[ %s ] %s')));
  process.chdir(dir);
};
export var realpath = fs.realpathSync;
export var env = recurry(2)(function (key) {
  return function (val) {
    ;
    pipe(pipe([brightRed(key), val], sprintfN('[ env ] %s %s')), log);
    process.env[key] = val;
  };
});
export var coerceDirOk = tap(fs.opendirSync);
export var coerceFileOk = tap(fs.openSync);
export var coerceCreateDirIfNotExists = tap(function (dir) {
  if (!fs.existsSync(dir)) return fs.mkdirSync(dir);
  if (fs.lstatSync(dir).isDirectory()) return;
  error(dir + ' exists but is not a directory');
});
export var coerceFileMustNotExist = tap(function (file) {
  return pipe(fs.existsSync(file), whenTrue(function () {
    return error(file + ' exists');
  }));
});
export var coerceDirMustNotExist = coerceFileMustNotExist;