#!/usr/bin/env node

import {
  pipe, compose, composeRight,
  die, sprintf1, sprintfN, recurry, whenOk, tap,
  ifTrue, id, concatTo, ifOk,
} from 'stick-js/es'

import fs from 'fs'
import { join as pathJoin, } from 'path'

import { flatMap, } from 'alleycat-js/es/bilby'

import yargsMod from 'yargs'

import {
  bindir, brightRed, chdir,
  coerceDirOk, coerceFileOk, coerceCreateDirIfNotExists,
  coerceDirMustNotExist, coerceFileMustNotExist,
  env, error, info, log, realpath, sys1, yellow,
} from './util.mjs'

const flatten = flatMap (id)

// --- like `whenOk` but return an empty list on nil.
const whenOkList = recurry (2) (
  (yes) => ifOk (yes, () => []),
)

const rootDir = pathJoin (bindir, '../..')
const appExample = pathJoin (rootDir, 'app-example')
const configExample = pathJoin (rootDir, 'config-example.mjs')
// --- we change directory to here so that the right babel.config.js gets used.
// --- @todo app-babel is a terrible name
const runDir = pathJoin (rootDir, 'app-babel')
const mainlibDir = pathJoin (rootDir, 'main/lib')
const webpackConfigDevserver = pathJoin (mainlibDir, 'webpack/webpack.devserver.babel.mjs')
const webpackConfigBuild = pathJoin (mainlibDir, 'webpack/webpack.build.babel.mjs')
const plopfile = pathJoin (mainlibDir, 'generators/index.mjs')

const cmd = (argv, next, doChdir=true) => {
  const whenOkSetEnv = (key) => whenOk (env (key))
  argv.appDir     | whenOkSetEnv ('APP_DIR')
  argv.buildDir   | whenOkSetEnv ('BUILD_DIR')
  argv.configFile | whenOkSetEnv ('AC_CONFIG_FILE')
  if (doChdir) chdir (runDir)
  next (argv)
}

const _runDevServer = (host=null, port=null) => sys1 (
  ... flatten ([
    ['npx', 'webpack', 'serve', '--config', webpackConfigDevserver],
    ... port | whenOkList (concatTo (['--port'])),
    ... host | whenOkList (concatTo (['--host'])),
  ])
)

const cmdInit = (argv) => {
  sys1 ('cp', '-ar', appExample, argv.appDir)
  info (yellow (argv.appDir) | sprintf1 ('created %s'))
  argv.withExample | ifTrue (
    () => {
      sys1 ('cp', '-a', configExample, argv.configFile)
    },
    () => {
      const main = pathJoin (argv.appDir, 'containers/Main/index.js')
      fs.writeFileSync (argv.configFile, 'export default {}')
      sys1 ('rm', '-rf', pathJoin (argv.appDir, 'containers/Example'))
      // sys1 ('sed', '-i', '-e', 's,.\*import.\\+containers/Example.\\+,,', main)
      // sys1 ('sed', '-i', '-e', 's,.\*<Example.\\+,,', main)
      sys1 ('sed', '-i', '-e', '/import.\\+containers\\/Example/d;', main)
      sys1 ('sed', '-i', '-e', '/<Example/d', main)
    }
  )
  info (yellow (argv.configFile) | sprintf1 ('created %s'))
}

const cmdDevServer = (argv) => {
  env ('WEBPACK_TYPE', 'dev')
  env ('NODE_ENV', 'development')
  _runDevServer (argv.host, argv.port)
}

const cmdDevServerPrd = (argv) => {
  env ('WEBPACK_TYPE', 'dev')
  env ('NODE_ENV', 'production')
  _runDevServer ()
}

const cmdBuild = (whichEnv) => (argv) => {
  env ('APP_ENV', whichEnv)

  env ('NODE_ENV', 'production')
  env ('NODE_OPTIONS', '--trace-deprecation')
  env ('WEBPACK_TYPE', 'build')
  sys1 ('npx', 'webpack', '--config', webpackConfigBuild, '--color', '--progress')
}

const generate = (argv) => {
  const appDir = realpath (argv.appDir)
  sys1 ('npx', 'plop', '--plopfile', plopfile, '--dest', appDir)
}

const yargs = yargsMod (process.argv.slice (2))
  .usage (`Usage: node $0 {command} [options]`)
  .strict ()
  .help ('h')
  .alias ('h', 'help')
  .showHelpOnFail (false, 'Specify --help for available options')
  .command ({
    command: '$0',
    handler: (argv) => yargs.showHelp (error),
  })

const addOptsInit = (yargs) => yargs
  .option ('app-dir', {
    describe: `app dir to create, must not exist, e.g. 'app'`,
    demandOption: true,
    coerce: coerceDirMustNotExist,
  })
  .option ('config-file', {
    describe: `config file to create, must not exist, e.g. 'config.mjs'`,
    demandOption: true,
    coerce: coerceFileMustNotExist,
  })

const addOptsNonInit = (yargs) => yargs
  .option ('app-dir', {
    describe: `app dir`,
    demandOption: true,
    coerce: coerceDirOk >> realpath,
  })
  .option ('config-file', {
    describe: `config file (e.g. config.mjs)`,
    demandOption: true,
    coerce: coerceFileOk >> realpath,
  })

const optsDev = {
  builder: (yargs) => (yargs | addOptsNonInit)
    .option ('port', {
      describe: 'port',
      default: 3000,
      number: true,
    })
    .option ('host', {
      describe: 'host',
    }),
}

const optsBuild = {
  builder: (yargs) => (yargs | addOptsNonInit)
    .option ('build-dir', {
      describe: 'the directory in which to build (will be created if necessary)',
      demandOption: true,
      coerce: coerceCreateDirIfNotExists >> realpath,
    })
}

yargs
  .command ({
    command: 'init',
    describe: 'initialize your project',
    builder: (yargs) => (yargs | addOptsInit)
      .option ('with-example', {
        describe: 'include <Example/> component and API call',
        boolean: true,
      }),
    handler: (argv) => cmdInit (argv),
  })
  .command ({
    command: 'devserver',
    describe: 'run the dev server',
    handler: (argv) => cmd (argv, cmdDevServer),
    ... optsDev,
  })
  .command ({
    command: 'devserver-prd',
    describe: 'run the dev server in production mode',
    handler: (argv) => cmd (argv, cmdDevServerPrd),
    ... optsDev,
  })
  .command ({
    command: 'build-tst',
    describe: 'build the app in testing mode',
    handler: (argv) => cmd (argv, cmdBuild ('tst')),
    ... optsBuild,
  })
  .command ({
    command: 'build-acc',
    describe: 'build the app in acceptance mode',
    handler: (argv) => cmd (argv, cmdBuild ('acc')),
    ... optsBuild,
  })
  .command ({
    command: 'build-prd',
    describe: 'build the app in production mode',
    handler: (argv) => cmd (argv, cmdBuild ('prd')),
    ... optsBuild,
  })
  .command ({
    command: 'build-prd-profile',
    describe: 'build the app in production mode with profiling',
    handler: (argv) => cmd (argv, cmdBuild ('prd-profile')),
    ... optsBuild,
  })
  .command ({
    command: 'generate',
    describe: 'add new components/containers',
    handler: (argv) => cmd (argv, generate),
    builder: (yargs) => yargs | addOptsNonInit,
  })

  // --- important: runs yargs
  .argv
