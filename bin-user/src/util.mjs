import {
  pipe, compose, composeRight,
  sprintfN, recurry, tap,
  whenTrue, die,
} from 'stick-js/es'

import fs from 'fs'
import { dirname, } from 'path'
import { fileURLToPath, } from 'url'

import fishLib from 'fish-lib'

const {
  log, info, warn, error, green, yellow, magenta, brightRed, cyan, brightBlue,
  sprintf, getopt, bulletSet, sysSpawn,
} = fishLib.forceColors ()

export {
  log, info, warn, error, green, yellow, magenta, brightRed, cyan, brightBlue,
  sprintf, getopt, bulletSet, sysSpawn,
}

bulletSet ({ type: 'star', })

export const bindir = dirname (fileURLToPath (import.meta.url))

// --- prints continuously to stdout
export const sys1 = (prog, ...args) => sysSpawn (
  prog, args,
  { die: true, sync: true, verbose: true, outPrint: true, },
)

export const chdir = (dir) => {
  info ([yellow ('chdir'), dir] | sprintfN ('[ %s ] %s'))
  process.chdir (dir)
}

export const realpath = fs.realpathSync

export const env = recurry (2) (
  (key) => (val) => {
    ; [brightRed (key), val]
    | sprintfN ('[ env ] %s %s')
    | log
  process.env [key] = val
})

// --- yargs
export const coerceDirOk = tap (fs.opendirSync)
export const coerceFileOk = tap (fs.openSync)
export const coerceCreateDirIfNotExists = tap ((dir) => {
  if (!fs.existsSync (dir)) return fs.mkdirSync (dir)
  if (fs.lstatSync (dir).isDirectory ()) return
  error (dir + ' exists but is not a directory')
})
export const coerceFileMustNotExist = tap (
  (file) => fs.existsSync (file) | whenTrue (
    () => error (file + ' exists'),
  ),
)
export const coerceDirMustNotExist = coerceFileMustNotExist
