#!/usr/bin/env bash

set -eu
set -o pipefail

# --- keeps colors on when running background processes
set -m

bindir=$(realpath "$(dirname "$0")")
rootdir="$bindir"/..
toolsdir="$rootdir"/tools
maindir="$rootdir"/main
nodemodulesdir="$rootdir"/node_modules
# nodemodulesdir=~/volume1/src/oton/node_modules
nodemodulesbindir="$nodemodulesdir"/.bin
babelcmd="$nodemodulesbindir"/babel

. "$bindir"/functions.bash

USAGE="Usage: $0 [-w]"

opt_w=
while getopts hw-: arg; do
    case $arg in
        h) warn "$USAGE"; exit 0 ;;
        w) opt_w=yes ;;
        -) OPTARG_VALUE="${OPTARG#*=}"
            case $OPTARG in
                help)  warn "$USAGE"; exit 0 ;;
                '')    break ;;
                *)     error "Illegal option --$OPTARG" ;;
                esac ;;
        *) error "$USAGE" ;;
    esac
done
shift $((OPTIND-1))

_ret=

babel-cmd () {
    local watch=$1; shift
    local opts=(
      --keep-file-extension
      # --compact
      --no-comments
    )
    if [ "$watch" = yes ]; then
        opts+=(-w)
    fi
    cmd "$babelcmd" "${opts[@]}" "$@"
}

# --- @todo deprecate tools

build-tools () {
    local watch=$1; shift
    cwd "$toolsdir" fun babel-cmd "$watch" -d lib src
}

build-main () {
    local watch=$1; shift
    # --- we want cjs for now: we are building (among other things) the webpack config that will be
    # passed to webpack --config <file> and that doesn't support mjs yet.
    # --- so we do for example `import { ... } from 'alleycat-js'`, and that gets turned into require
    # by babel, and then cjs is triggered for stick-js as well.
    # stack-push-xport BABEL_ENV cjs
    cwd "$maindir" fun babel-cmd "$watch" -d lib src
    # stack-pop BABEL_ENV
}

go () {
    local watch=$1

    info Building tools
    fun build-tools no

    info Building main
    fun build-main "$watch"
}

fun go "$opt_w"
