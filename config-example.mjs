export default {
  frontendDevProxy: [
    ['/api/osm', {
      target: 'https://www.openstreetmap.org/api/0.6',
      logLevel: 'debug',
      changeOrigin: true,
      pathRewrite: { '^/api/osm': '' },
    }],
  ],
}
