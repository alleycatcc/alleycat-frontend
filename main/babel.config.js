const config = {
  presets: [
    [
      '@babel/env',
      {
        modules: false,
      },
    ],
  ],
  plugins: [
    'alleycat-stick-transforms',
    // --- reduce code size and avoid namespace pollution (e.g. global
    // polyfills; be sure to add @babel/runtime to runtime deps)
    '@babel/transform-runtime',
  ],
}

module.exports = (api) => {
  api.cache.forever ()
  return config
}
