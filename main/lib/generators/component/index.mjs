import _toConsumableArray from "@babel/runtime/helpers/toConsumableArray";
import { pipe, compose, composeRight, guardV, otherwise, eq, condS, guard, T, tap, lets } from 'stick-js/es';
import { whenNotEquals } from 'alleycat-js/es/predicate';
import { componentExists, pascalCase, pathJoin, spreadWhen } from '../util.mjs';
var templateComponent = '../../templates/component/component.js.hbs';
var templateLoadable = '../../templates/common/loadable.js.hbs';
export var generator = function generator(destPath) {
  return {
    description: 'Add a component',
    prompts: [{
      type: 'input',
      name: 'name',
      message: 'What should it be called?',
      "default": 'Vliegtuig',
      validate: function validate(value) {
        return pipe(value, condS([pipe(pipe('', eq), guardV('The name is required')), pipe(componentExists(destPath), guardV('A component or container with this name already exists')), pipe(otherwise, guard(T))]));
      },
      filter: function filter(value) {
        return pipe(pascalCase(value), tap(whenNotEquals(value)(function (pc) {
          return console.log('\nconverting to PascalCase ->', pc);
        })));
      }
    }, {
      type: 'confirm',
      name: 'wantLoadable',
      "default": true,
      message: 'Do you want to use lazy loading (i.e. code splitting)?'
    }],
    actions: function actions(_ref) {
      var name = _ref.name,
        wantLoadable = _ref.wantLoadable;
      var path = pathJoin(destPath, 'components', name);
      console.log('path', path);
      return [{
        type: 'add',
        path: pathJoin(path, 'index.js'),
        templateFile: templateComponent,
        abortOnFail: true
      }].concat(_toConsumableArray(spreadWhen(wantLoadable, [{
        type: 'add',
        path: pathJoin(path, 'Loadable.js'),
        templateFile: templateLoadable,
        abortOnFail: true
      }])));
    }
  };
};