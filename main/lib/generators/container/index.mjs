import _toConsumableArray from "@babel/runtime/helpers/toConsumableArray";
import { pipe, compose, composeRight, guardV, otherwise, eq, condS, guard, T, sprintf1, tap } from 'stick-js/es';
import { whenNotEquals } from 'alleycat-js/es/predicate';
import { componentExists, pascalCase, pathJoin, spreadWhen } from '../util.mjs';
var template = sprintf1('../../templates/container/%s.js.hbs');
var templateLoadable = '../../templates/common/loadable.js.hbs';
export var generator = function generator(destPath) {
  return {
    description: 'Add a container',
    prompts: [{
      type: 'input',
      name: 'name',
      message: 'What should it be called?',
      "default": 'SlimmePeer',
      validate: function validate(value) {
        return pipe(value, condS([pipe(pipe('', eq), guardV('The name is required')), pipe(componentExists(destPath), guardV('A component or container with this name already exists')), pipe(otherwise, guard(T))]));
      },
      filter: function filter(value) {
        return pipe(pascalCase(value), tap(whenNotEquals(value)(function (pc) {
          return console.log('\nconverting to PascalCase ->', pc);
        })));
      }
    }, {
      type: 'confirm',
      name: 'wantLoadable',
      "default": true,
      message: 'Do you want to use lazy loading (i.e. code splitting)?'
    }],
    actions: function actions(_ref) {
      var wantLoadable = _ref.wantLoadable,
        name = _ref.name;
      var path = pathJoin(destPath, 'containers', name);
      return [{
        type: 'add',
        path: pathJoin(path, 'actions.js'),
        templateFile: template('actions'),
        abortOnFail: true
      }, {
        type: 'add',
        path: pathJoin(path, 'index.js'),
        templateFile: template('container'),
        abortOnFail: true
      }, {
        type: 'add',
        path: pathJoin(path, 'reducer.js'),
        templateFile: template('reducer'),
        abortOnFail: true
      }, {
        type: 'add',
        path: pathJoin(path, 'saga.js'),
        templateFile: template('saga'),
        abortOnFail: true
      }, {
        type: 'add',
        path: pathJoin(path, 'selectors.js'),
        templateFile: template('selectors'),
        abortOnFail: true
      }].concat(_toConsumableArray(spreadWhen(wantLoadable, [{
        type: 'add',
        path: pathJoin(path, 'Loadable.js'),
        templateFile: templateLoadable,
        abortOnFail: true
      }])));
    }
  };
};