import { pipe, compose, composeRight, sprintf1, side2, invoke, concatTo, die } from 'stick-js/es';
import fs from 'fs';
import { generator as componentGenerator } from './component/index.mjs';
import { generator as containerGenerator } from './container/index.mjs';
import { ifExists } from './util.mjs';
var setGenerator = side2('setGenerator');
var addHelper = side2('addHelper');
export default (function (plop, _ref) {
  var destBasePath = _ref.destBasePath;
  return pipe(destBasePath, ifExists(function () {
    return pipe(pipe(plop, setGenerator('component', componentGenerator(destBasePath))), setGenerator('container', containerGenerator(destBasePath)));
  }, function () {
    return die(pipe(destBasePath, sprintf1('Invalid destination path (%s)')));
  }));
});