import { pipe, compose, composeRight, recurry, lets, find, map, eq, ifPredicate, passToN, concatTo } from 'stick-js/es';
import fs from 'fs';
import { dirname, join as pathJoin } from 'path';
import { fileURLToPath } from 'url';
import * as changeCase from 'change-case';
var pascalCase = changeCase.pascalCase;
export { pascalCase };
var flatMap = recurry(2)(function (f) {
  return function (xs) {
    return xs.flatMap(function (y) {
      return f(y);
    });
  };
});
var filenames = function filenames(thePath) {
  return pipe([pathJoin(thePath, 'components'), pathJoin(thePath, 'containers')], flatMap(fs.readdirSync));
};
export var componentExists = recurry(2)(function (appPath) {
  return function (name) {
    return lets(function () {
      return filenames(appPath);
    }, function (names) {
      return pipe(names, find(pipe(name, eq)));
    });
  };
});
export { pathJoin };
export var pathJoinN = passToN(pathJoin);
var exists = fs.existsSync;
export var ifExists = pipe(exists, ifPredicate);
export var spreadWhen = recurry(2)(function (pv) {
  return function (yes) {
    return pv ? yes : [];
  };
});
export var __dirname = composeRight(fileURLToPath, dirname);