import { pipe, compose, composeRight, ok, ifOk, ifPredicate, whenOk, whenPredicate, defaultTo, list, id, tap, recurry, map, filter, reject, reduce, flip, flip3, join, split, last, head, tail, dot, dot1, dot2, side, side1, side2, cond, condS, guard, guardV, otherwise, sprintf1, sprintfN, noop, blush, always, T, F, prop, concat, lets, letS, die, eq, ne, factory, factoryProps, mergeTo } from 'stick-js';
import path from 'path';
import webpack from 'webpack';
export var pathJoin = composeRight(list, join(path.sep));
export var appDir = pipe(process.env.APP_DIR, defaultTo(function () {
  return die('missing APP_DIR in env');
}));
var OFFLINE_PLUGIN = false;
process.env.OFFLINE_PLUGIN = OFFLINE_PLUGIN;
var basePlugins = [new webpack.DefinePlugin({
  'process.env': {
    NODE_ENV: JSON.stringify(process.env.NODE_ENV),
    APP_ENV: JSON.stringify(process.env.APP_ENV),
    OFFLINE_PLUGIN: JSON.stringify(OFFLINE_PLUGIN)
  }
})];
export var config = function config(_ref) {
  var mode = _ref.mode,
    entry = _ref.entry,
    output = _ref.output,
    optimization = _ref.optimization,
    plugins = _ref.plugins,
    devServer = _ref.devServer,
    devtool = _ref.devtool,
    _ref$performance = _ref.performance,
    performance = _ref$performance === void 0 ? {} : _ref$performance,
    _ref$babelOptions = _ref.babelOptions,
    babelOptions = _ref$babelOptions === void 0 ? {} : _ref$babelOptions;
  return {
    mode: mode,
    entry: entry,
    optimization: optimization,
    devServer: devServer,
    devtool: devtool,
    performance: performance,
    target: 'web',
    output: pipe(output, mergeTo({
      publicPath: '/',
      hashFunction: 'xxhash64'
    })),
    plugins: pipe(plugins, concat(basePlugins)),
    resolve: {
      modules: ['node_modules'],
      extensions: ['.js', '.jsx'],
      mainFields: ['module', 'browser', 'jsnext:main', 'main']
    },
    module: {
      rules: [{
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: babelOptions
        }
      }, {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }, {
        test: /\.(eot|otf|ttf|woff|woff2|ogg|mp3)$/,
        type: 'asset'
      }, {
        test: /favicon\.ico$/,
        type: 'asset/resource',
        generator: {
          filename: '[name][ext]'
        }
      }, {
        test: /icons-\d+\.png$/,
        type: 'asset/resource',
        generator: {
          filename: '[name][ext]'
        }
      }, {
        test: /static-.+$/,
        type: 'asset/resource',
        generator: {
          filename: function filename(pathData, _assetInfo) {
            var filename = pathData.filename;
            return path.basename(filename).replace(/^static-/, '');
          }
        }
      }, {
        test: /\.(cur|png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource'
      }, {
        test: /manifest\.json/,
        type: 'asset/resource',
        exclude: /node_modules/,
        generator: {
          filename: 'manifest.json'
        }
      }]
    }
  };
};