import { pipe, compose, composeRight, tap, concatTo, lets, dot2, xMatchStr, prop, ifNo, defaultTo, die } from 'stick-js/es';
import path from 'path';
import CompressionPlugin from 'compression-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import { config as baseConfig, pathJoin, appDir } from './webpack.base.babel.mjs';
var replace = pipe('replace', dot2);
var buildDir = pipe(process.env.BUILD_DIR, defaultTo(function () {
  return die('missing BUILD_DIR in env');
}));
var APP_ENV = process.env.APP_ENV;
var plugins = [new HtmlWebpackPlugin({
  template: pathJoin(appDir, 'index.html'),
  minify: {
    removeComments: true,
    collapseWhitespace: true,
    removeRedundantAttributes: true,
    useShortDoctype: true,
    removeEmptyAttributes: true,
    removeStyleLinkTypeAttributes: true,
    keepClosingSlash: true,
    minifyJS: true,
    minifyCSS: true,
    minifyURLs: true
  },
  inject: true
}), new CompressionPlugin({
  algorithm: 'gzip',
  test: /\.js$|\.css$|\.html$/,
  threshold: 10240,
  minRatio: 0.8
})];
export default baseConfig({
  plugins: plugins,
  mode: 'production',
  entry: [pathJoin(appDir, 'app.js')],
  output: {
    path: buildDir,
    filename: '[name].[chunkhash].js'
  },
  optimization: {
    minimize: true,
    nodeEnv: 'production',
    sideEffects: true,
    concatenateModules: true,
    runtimeChunk: 'single',
    moduleIds: 'deterministic',
    splitChunks: {
      chunks: 'all',
      maxInitialRequests: 10,
      minSize: 0,
      cacheGroups: {
        defaultVendors: {
          test: RegExp('/node_modules/'),
          name: function name(_ref) {
            var context = _ref.context;
            return lets(function () {
              return pipe(context, xMatchStr('/node_modules/ (.*?) (/|$)'));
            }, function (packageMatch) {
              return pipe(pipe(packageMatch, prop(1)), replace('@', ''));
            }, function (_, x) {
              return pipe(x, concatTo('npm.'));
            });
          }
        }
      }
    }
  },
  performance: {
    assetFilter: function assetFilter(assetFilename) {
      return !/(\.map$)|(^(main\.|favicon\.))/.test(assetFilename);
    }
  }
});