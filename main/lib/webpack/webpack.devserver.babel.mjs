var _acConfig$default$dev;
import { pipe, compose, composeRight, die, fromPairs, lets } from 'stick-js/es';
import path from 'path';
import CircularDependencyPlugin from 'circular-dependency-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import ReactRefreshWebpackPlugin from '@pmmmwh/react-refresh-webpack-plugin';
import { config as baseConfig, appDir, pathJoin } from './webpack.base.babel.mjs';
var configFile = process.env.AC_CONFIG_FILE || die('missing AC_CONFIG_FILE in env');
var acConfig = await import(configFile);
var proxy = lets(function () {
  return acConfig["default"].frontendDevProxy;
}, function () {
  var p = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  return pipe(p, fromPairs);
});
var devServerClient = (_acConfig$default$dev = acConfig["default"].devServerClient) !== null && _acConfig$default$dev !== void 0 ? _acConfig$default$dev : false;
var plugins = [new ReactRefreshWebpackPlugin(), new HtmlWebpackPlugin({
  inject: true,
  template: pathJoin(appDir, 'index.html')
}), new CircularDependencyPlugin({
  exclude: /a\.js|node_modules/,
  failOnError: false
})];
export default baseConfig({
  plugins: plugins,
  mode: 'development',
  devServer: {
    proxy: proxy,
    hot: true,
    historyApiFallback: true,
    client: devServerClient
  },
  babelOptions: {
    plugins: ['react-refresh/babel']
  },
  entry: {
    main: pathJoin(appDir, 'app.js'),
    'vendors-react': ['react', 'react-dom', 'react-refresh/runtime']
  },
  output: {
    filename: '[name].js',
    chunkFilename: '[name].chunk.js'
  },
  optimization: {
    runtimeChunk: 'single'
  },
  devtool: false,
  performance: {
    hints: false
  }
});