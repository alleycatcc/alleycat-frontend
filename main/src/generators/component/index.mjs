import {
  pipe, compose, composeRight,
  guardV, otherwise, eq, condS,
  guard, T, tap, lets,
} from 'stick-js/es'

import { whenNotEquals, } from 'alleycat-js/es/predicate'

import { componentExists, pascalCase, pathJoin, spreadWhen, } from '../util.mjs'

const templateComponent = '../../templates/component/component.js.hbs'
const templateLoadable = '../../templates/common/loadable.js.hbs'

export const generator = (destPath) => ({
  description: 'Add a component',
  prompts: [
    {
      type: 'input',
      name: 'name',
      message: 'What should it be called?',
      default: 'Vliegtuig',
      validate: (value) => value | condS ([
        '' | eq | guardV ('The name is required'),
        componentExists (destPath) | guardV ('A component or container with this name already exists'),
        otherwise | guard (T),
      ]),
      filter: (value) => pascalCase (value) | tap (
        whenNotEquals (value) ((pc) => console.log ('\nconverting to PascalCase ->', pc))
      ),
    },
    {
      type: 'confirm',
      name: 'wantLoadable',
      default: true,
      message: 'Do you want to use lazy loading (i.e. code splitting)?',
    },
  ],
  actions: ({ name, wantLoadable, }) => {
    const path = pathJoin (destPath, 'components', name)
    console.log ('path', path)
    return [
      {
        type: 'add',
        path: pathJoin (path, 'index.js'),
        templateFile: templateComponent,
        abortOnFail: true,
      },
      ... spreadWhen (wantLoadable, [{
        type: 'add',
        path: pathJoin (path, 'Loadable.js'),
        templateFile: templateLoadable,
        abortOnFail: true,
      },
    ]),
  ]
}})
