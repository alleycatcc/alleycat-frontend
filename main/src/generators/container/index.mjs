import {
  pipe, compose, composeRight,
  guardV, otherwise, eq, condS,
  guard, T, sprintf1, tap,
} from 'stick-js/es'

import { whenNotEquals, } from 'alleycat-js/es/predicate'

import { componentExists, pascalCase, pathJoin, spreadWhen, } from '../util.mjs'

const template = sprintf1 ('../../templates/container/%s.js.hbs')
const templateLoadable = '../../templates/common/loadable.js.hbs'

export const generator = (destPath) => ({
  description: 'Add a container',
  prompts: [
    {
      type: 'input',
      name: 'name',
      message: 'What should it be called?',
      default: 'SlimmePeer',
      validate: value => value | condS ([
        '' | eq | guardV ('The name is required'),
        componentExists (destPath) | guardV ('A component or container with this name already exists'),
        otherwise | guard (T),
      ]),
      filter: (value) => pascalCase (value) | tap (
        whenNotEquals (value) ((pc) => console.log ('\nconverting to PascalCase ->', pc))
      ),
    },
    {
      type: 'confirm',
      name: 'wantLoadable',
      default: true,
      message: 'Do you want to use lazy loading (i.e. code splitting)?',
    },
  ],
  actions: ({ wantLoadable, name, }) => {
    const path = pathJoin (destPath, 'containers', name)
    return [
      {
        type: 'add',
        path: pathJoin (path, 'actions.js'),
        templateFile: template ('actions'),
        abortOnFail: true,
      },
      {
        type: 'add',
        path: pathJoin (path, 'index.js'),
        templateFile: template ('container'),
        abortOnFail: true,
      },
      {
        type: 'add',
        path: pathJoin (path, 'reducer.js'),
        templateFile: template ('reducer'),
        abortOnFail: true,
      },
      {
        type: 'add',
        path: pathJoin (path, 'saga.js'),
        templateFile: template ('saga'),
        abortOnFail: true,
      },
      {
        type: 'add',
        path: pathJoin (path, 'selectors.js'),
        templateFile: template ('selectors'),
        abortOnFail: true,
      },
      ... spreadWhen (wantLoadable, [{
        type: 'add',
        path: pathJoin (path, 'Loadable.js'),
        templateFile: templateLoadable,
        abortOnFail: true,
      }]),
    ]
  }
})
