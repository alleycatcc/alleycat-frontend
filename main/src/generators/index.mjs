import {
  pipe, compose, composeRight,
  sprintf1, side2, invoke, concatTo, die,
} from 'stick-js/es'

import fs from 'fs'

import { generator as componentGenerator, } from './component/index.mjs'
import { generator as containerGenerator, } from './container/index.mjs'
import { ifExists, } from './util.mjs'

const setGenerator = side2 ('setGenerator')
const addHelper = side2 ('addHelper')

// --- `destBasePath` is set by passing --dest to the plop command
export default (plop, { destBasePath, }) => destBasePath | ifExists (
  () => plop
    | setGenerator ('component', componentGenerator (destBasePath))
    | setGenerator ('container', containerGenerator (destBasePath)),
  () => die (destBasePath | sprintf1 ('Invalid destination path (%s)')),
)
