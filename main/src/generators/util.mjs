import {
  pipe, compose, composeRight,
  recurry, lets, find,
  map, eq,
  ifPredicate, passToN, concatTo,
} from 'stick-js/es'

import fs from 'fs'
import { dirname, join as pathJoin, } from 'path'
import { fileURLToPath, } from 'url'

import * as changeCase from 'change-case'
const { pascalCase, } = changeCase
export { pascalCase, }

const flatMap = recurry (2) (
  (f) => (xs) => xs.flatMap ((y) => f (y)),
)

const filenames = (thePath) => [
  pathJoin (thePath, 'components'),
  pathJoin (thePath, 'containers'),
] | flatMap (fs.readdirSync)

export const componentExists = recurry (2) (
  (appPath) => (name) => lets (
    () => filenames (appPath),
    (names) => names | find (name | eq),
  ),
)

export { pathJoin, }
export const pathJoinN = passToN (pathJoin)

const exists = fs.existsSync

export const ifExists = exists | ifPredicate

export const spreadWhen = recurry (2) (
  (pv) => (yes) => pv ? yes : [],
)

// --- usage: `__dirname (import.meta.url)`
export const __dirname = fileURLToPath >> dirname
