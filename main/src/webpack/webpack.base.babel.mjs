import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  defaultTo, list,
  id, tap, recurry,
  map, filter, reject, reduce, flip, flip3,
  join, split, last, head, tail,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN,
  noop, blush, always, T, F,
  prop,
  concat,
  lets, letS,
  die, eq, ne,
  factory, factoryProps,
  mergeTo,
} from 'stick-js'

// --- old, but may require tweaking or reviving in the case of things like the passphrases app
// (i.e. a bundle retrieved via file:/// urls, a webview component, etc.)
// const useRelativePath = process.env.WEBPACK_TYPE === 'build'

import path from 'path'

import webpack from 'webpack'

// --- path.join removes a leading dot, which is wrong.
export const pathJoin = list >> join (path.sep)

export const appDir = process.env.APP_DIR | defaultTo (() => die ('missing APP_DIR in env'))

const OFFLINE_PLUGIN = false
process.env.OFFLINE_PLUGIN = OFFLINE_PLUGIN

const basePlugins = [
  new webpack.DefinePlugin ({
    'process.env': {
      NODE_ENV: JSON.stringify (process.env.NODE_ENV),
      APP_ENV: JSON.stringify (process.env.APP_ENV),
      OFFLINE_PLUGIN: JSON.stringify (OFFLINE_PLUGIN),
    },
  }),
]

export const config = ({
  mode, entry, output, optimization, plugins, devServer, devtool, performance={},
  babelOptions={},
}) => ({
  mode,
  entry,
  optimization,
  devServer,
  devtool,
  performance,

  // --- gives us `window` var
  target: 'web',

  output: output | mergeTo ({
    publicPath: '/',
    hashFunction: 'xxhash64',
  }),

  plugins: plugins | concat (basePlugins),

  resolve: {
    modules: ['node_modules'],
    extensions: ['.js', '.jsx'],
    // --- @todo try 'exports'
    // --- note: keep 'module' before the rest.
    mainFields: ['module', 'browser', 'jsnext:main', 'main'],
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: babelOptions,
        },
      },

      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(eot|otf|ttf|woff|woff2|ogg|mp3)$/,
        type: 'asset',
      },
      {
        test: /favicon\.ico$/,
        type: 'asset/resource',
        generator: { filename: '[name][ext]' },
      },
      // --- @todo remove this
      {
        test: /icons-\d+\.png$/,
        type: 'asset/resource',
        generator: { filename: '[name][ext]' },
      },
      {
        test: /static-.+$/,
        type: 'asset/resource',
        generator: {
          filename: (pathData, _assetInfo) => {
            const { filename, } = pathData
            return path.basename (filename).replace (/^static-/, '')
          },
        },
      },
      {
        test: /\.(cur|png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
      },
      {
        test: /manifest\.json/,
        type: 'asset/resource',
        exclude: /node_modules/,
        generator: { filename: 'manifest.json' },
      },
    ],
  },
})
