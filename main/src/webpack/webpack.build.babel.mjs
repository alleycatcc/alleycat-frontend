import {
  pipe, compose, composeRight,
  tap, concatTo, lets,
  dot2, xMatchStr, prop,
  ifNo, defaultTo, die,
} from 'stick-js/es'

import path from 'path'

import CompressionPlugin from 'compression-webpack-plugin'
import HtmlWebpackPlugin from 'html-webpack-plugin'

import { config as baseConfig, pathJoin, appDir, } from './webpack.base.babel.mjs'

const replace = 'replace' | dot2

const buildDir = process.env.BUILD_DIR | defaultTo (() => die ('missing BUILD_DIR in env'))

const { APP_ENV, } = process.env

const plugins = [
  new HtmlWebpackPlugin ({
    template: pathJoin (appDir, 'index.html'),
    minify: {
      removeComments: true,
      collapseWhitespace: true,
      removeRedundantAttributes: true,
      useShortDoctype: true,
      removeEmptyAttributes: true,
      removeStyleLinkTypeAttributes: true,
      keepClosingSlash: true,
      minifyJS: true,
      minifyCSS: true,
      minifyURLs: true,
    },
    inject: true,
  }),

  // --- necessary? the server is set to gzip responses as well; probably doesn't have much effect
  // to do it twice.
  new CompressionPlugin ({
	algorithm: 'gzip',
	test: /\.js$|\.css$|\.html$/,
	threshold: 10240,
	minRatio: 0.8,
  }),
]

export default baseConfig ({
  plugins,

  // --- automatically configures DefinePlugin and sets NODE_ENV to production.
  mode: 'production',

  entry: [pathJoin (appDir, 'app.js')],

  // --- react-boilerplate
  // use content hash ('chunkhash') instead of compilation hash to improve caching.
  output: {
    path: buildDir,
    filename: '[name].[chunkhash].js',
  },

  optimization: {
    minimize: true,
    nodeEnv: 'production',
    sideEffects: true,
    concatenateModules: true,
    runtimeChunk: 'single',
    moduleIds: 'deterministic',
    splitChunks: {
      chunks: 'all',
      maxInitialRequests: 10,
      minSize: 0,
      cacheGroups: {
        defaultVendors: {
          test: RegExp ('/node_modules/'),
          // --- given ramda/a, ramda/b, ramda, make a chunk called
          // npm.ramda.[hash].chunk.js[.gz]
          name: ({ context, }) => lets (
            () => context | xMatchStr (
              '/node_modules/ (.*?) (/|$)',
            ),
            (packageMatch) => packageMatch | prop (1) | replace ('@', ''),
            (_, x) => x | concatTo ('npm.'),
          ),
        },
      },
    },
  },

  // devtool: APP_ENV === 'tst' && 'eval-source-map',

  performance: {
    assetFilter: assetFilename =>
      !/(\.map$)|(^(main\.|favicon\.))/.test (assetFilename),
  },
})
