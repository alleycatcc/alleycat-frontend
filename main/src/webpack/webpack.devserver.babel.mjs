// ------ this is used by the dev server (i.e. yarn/npm run start)

import {
  pipe, compose, composeRight,
  die, fromPairs, lets,
} from 'stick-js/es'

import path from 'path'

import CircularDependencyPlugin from 'circular-dependency-plugin'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import ReactRefreshWebpackPlugin from '@pmmmwh/react-refresh-webpack-plugin'

// import webpack from 'webpack'

import { config as baseConfig, appDir, pathJoin, } from './webpack.base.babel.mjs'

const configFile = process.env.AC_CONFIG_FILE || die ('missing AC_CONFIG_FILE in env')
const acConfig = await import (configFile)
const proxy = lets (
  () => acConfig.default.frontendDevProxy,
  (p=[]) => p | fromPairs,
)
const devServerClient = acConfig.default.devServerClient ?? false

const plugins = [
  new ReactRefreshWebpackPlugin (),
  new HtmlWebpackPlugin ({
    inject: true,
    template: pathJoin (appDir, 'index.html'),
  }),
  new CircularDependencyPlugin ({
    // --- a.js: it seems you can name a module e.g. fooa.js to bypass this check if necessary.
    exclude: /a\.js|node_modules/,
    // --- only warn
    failOnError: false,
  }),
]

export default baseConfig ({
  plugins,

  mode: 'development',
  devServer: {
    proxy,
    hot: true,
    historyApiFallback: true,
    client: devServerClient,
  },
  babelOptions: {
    plugins: ['react-refresh/babel'],
  },
  entry: {
    main: pathJoin (appDir, 'app.js'),
    'vendors-react': ['react', 'react-dom', 'react-refresh/runtime'],
  },

  // --- react-boilerplate:
  // Don't use hashes in dev mode for better performance
  output: {
    filename: '[name].js',
    chunkFilename: '[name].chunk.js',
  },

  optimization: {
    runtimeChunk: 'single',
  },

  devtool: false,

  performance: {
    hints: false,
  },
})
