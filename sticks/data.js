import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap, recurry, roll,
  map, filter, reject, reduce, flip, flip3,
  join, split, last, head, tail,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN,
  noop, blush, always, T, F,
  prop, path, has, hasIn,
  bindPropTo, bindProp, bindTo, bind,
  assoc, assocPath, assocM, assocPathM,
  concatTo, concat, appendTo, append,
  concatToM, concatM, appendToM, appendM,
  merge, mergeTo, mergeM, mergeToM,
  mergeIn, mergeInTo, mergeInM, mergeInToM,
  updateM, update, updatePathM, updatePath,
  lets, letS, compactOk, compact,
  die, raise, decorateException, exception,
  lt, gt, eq, ne, lte, gte,
  factory, factoryProps,
  againstAny,
  againstEither,
  againstAll,
} from 'stick-js/es'

import daggy from './daggy'

import manual from './manual'

// --- alleycat-js
const all = (...fs) => {
  const acc = []
  let last
  for (const f of fs) {
    const ret = f (...acc)
    if (!ret) return false
    acc.push (ret)
    last = ret
  }
  return last
}

export const { cata, } = manual

export const methodX = condS

export const method = manual.method
export const method1 = recurry (3) (manual.method1)
export const method2 = recurry (4) (manual.method2)

export const addMethod = recurry (3) (manual.addMethod)
export const addMethod1 = recurry (3) (manual.addMethod1)
export const addMethod2 = recurry (3) (manual.addMethod2)

// --- note that Nothing, etc. require parentheses (done by commenting a section out of daggy.js)
export const adt = (name, constructors) => lets (
  _ => daggy.taggedSum (name, constructors),
  o => o | assocM (name, o) | tap ((o) => {
    o.prototype.valueOf = o.prototype.toString
  }),
)

// --- truthy, like guard.
export const dus = (f) => (pred) => [
  pred,
  // --- no result, maybe other function which also gives result.
  (target, result) => f (... target ['@@values']),
]

export const dusResult = (f) => (pred) => [
  pred,
  (target, result) => f (target ['@@values'], result),
]

export const is = type => o => type.is (o)

export const isEitherOf = (a, b) => againstEither (a | is, b | is)
export const isAny = map (is) >> againstAny

export const isAnyWith = recurry (3) (
  (f) => (types) => againstAll ([
    types[0] | is,
    (o) => f (o ['@@values']),
  ]),
)

export const isWith = recurry (3) (
  (f) => (type) => isAnyWith (f, [type]),
)

export const isAnyAnd = recurry (3) (
  (f) => (types) => againstAll ([
    types[0] | is,
    (o) => f (o),
  ]),
)

export const isAnd = recurry (3) (
  (f) => (type) => isAnyAnd (f, [type]),
)
