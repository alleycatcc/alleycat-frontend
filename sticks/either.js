import {
  pipe, compose, composeRight,
  id, tap,
  die,
} from 'stick-js/es'

import {
  adt,
  addMethod, addMethod1, addMethod2,
} from './data'

import {
  fold,
} from './prelude'

const { EitherT, Left, Right, } = adt ('EitherT', {
  Left: ['x'],
  Right: ['x'],
})

export { EitherT, Left, Right, }

export const fromLeft = fold (
  id,
  (x) => die (
    'fromLeft: not Left: ' + x.valueOf (),
  ),
)

export const fromRight = fold (
  (x) => die (
    'fromRight: not Right: ' + x.valueOf (),
  ),
  id,
)

EitherT | addMethod1 ('map', (f) => ({
  Left: composeRight (id, Left),
  Right: composeRight (f, Right),
}))

EitherT | addMethod1 ('flatMap', (f) => ({
  Left: id,
  Right: f,
}))

EitherT | addMethod1 ('ap', (f) => ({
  Left: x => f.cata ({
    Left: composeRight (id, Left),
    Right: _ => Left (x),
  }),
  Right: x => f.cata ({
    Left: composeRight (id, Left),
    Right: g => Right (g (x)),
  }),
}))

// @todo would be nice to just reverse the arguments and run `ap` somehow.
EitherT | addMethod1 ('apTo', (x) => ({
  Left: composeRight (id, Left),
  Right: f => x.cata ({
    Left: composeRight (id, Left),
    Right: y => Right (f (y)),
  }),
}))

EitherT | addMethod2 ('fold', (
  // currying is not necessary here, and manual currying is wrong.
  (f, g) => ({
    Left: f,
    Right: g,
  })
))

// --- toString is provided automatically by daggy and shows the constructor name and the vlaues.
// --- valueOf is set to be equal to toString by `adt`.
// --- set a custom valueOf to show a custom format.
EitherT | addMethod ('valueOf', () => ({
  Left: x => 'Left ' + JSON.stringify (x),
  Right: x => 'Right ' + JSON.stringify (x),
}))
