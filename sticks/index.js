import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap, recurry,
  map, filter, reject, reduce, flip, flip3,
  join, split, last, head, tail,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN,
  noop, blush, always, T, F,
  prop, path,
  lets, letS,
  die, eq, ne,
  factory, factoryProps,
} from 'stick-js/es'

export {
  cata, methodX, method, method1, method2, addMethod, addMethod1, addMethod2, adt,
  dus, dusResult, is, isEitherOf, isAny, isAnyWith, isWith, isAnyAnd, isAnd,
} from './data'
export { EitherT, Left, Right, fromLeft, fromRight, } from './either'
export { MaybeT, Just, Nothing, fromJust, } from './maybe'
export { fold, ap, apTo, toString, valueOf, flatMap, } from './prelude'

import { adt, } from './data'
import { toString, } from './prelude'

export const { Circle, Square, Blob, ShapeT, } = adt ('ShapeT', {
  Circle: ['radius'],
  Square: ['side'],
  Blob: [],
})

const { log, } = console

Circle | log
Circle (3) | toString | log

Blob | log
Blob () | toString | log
