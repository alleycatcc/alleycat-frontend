import {
  pipe, compose, composeRight,
  dot1,
} from 'stick-js/es'

export const curry = (() => {
  const _curry = (l, f) => function (...args) {
    const n = l - args.length
    if (n <= 0) return f.apply (this, args)
    return _curry (n, (...args2) =>
      f.apply (this, [...args, ...args2])
    )
  }

  return f => _curry (f.length, f)
}) ()

export const cata = dot1 ('cata')

export const method = f => (
  cata (f ())
)

export const method1 = (
  f => a => cata (f (a))
)

export const method2 = (
  f => a => b => cata (f (a, b))
)

/* The addMethod{n} functions wrap and return method{n}, but also add it to the prototype of ADT
 * object under the name `name`.
 *
 * This is important if you want to use a function like e.g. `map` on your data type, so that it
 * knows how to do the right thing. In a way this is how we can approximate the function of
 * typeclasses.
 *
 * @todo addMethodX{n}
 */
export const addMethod = (
  (name) => (f) => (O) => {
    const theMethod = method (f)
    O.prototype [name] = function () {
      return theMethod (this)
    }
    return O
  }
)

export const addMethod1 = (
  (name) => (f) => (O) => {
    const theMethod = method1 (f)
    O.prototype [name] = function (a) {
      return theMethod (a) (this)
    }
    return O
  }
)

export const addMethod2 = (
  (name) => (f) => (O) => {
    const theMethod = method2 (f)
    O.prototype [name] = curry (function (a, b) {
      return theMethod (a) (b) (this)
    })
    return O
  }
)

export default {
  cata,
  method, method1, method2,
  addMethod, addMethod1, addMethod2,
}
