import {
  pipe, compose, composeRight,
  id, die, always,
} from 'stick-js/es'

import {
  adt,
  addMethod, addMethod1, addMethod2,
  method1,
  method2,
} from './data'

import {
  fold,
} from './prelude'

const { MaybeT, Just, Nothing, } = adt ('MaybeT', {
  Just: ['x'],
  Nothing: [],
})

export {
  Just,
  MaybeT,
  Nothing,
}

export const fromJust = fold (
  id,
  (x) => die (
    'fromJust: not Just: ' + x.valueOf (),
  ),
)

MaybeT
  | addMethod1 ('map', (f) => ({
    Just: f >> Just,
    Nothing: Nothing,
  }))
  | addMethod1 ('flatMap', (f) => ({
    Just: f,
    Nothing: Nothing,
  }))
  // note that it's not necessary to curry here.
  // `fold` is curried by `dot2` and `.fold()` is curried by `addMethod2`.
  | addMethod2 ('fold', (f, g) => ({
    Just: f,
    Nothing: g,
  }))
  | addMethod1 ('ap', (mbf) => ({
    Just: (x) => mbf.fold (
      (f) => Just (f (x)),
      () => Nothing (),
    ),
    Nothing: Nothing,
  }))
  | addMethod1 ('apTo', (mbx) => ({
    Just: (f) => mbx.fold (
      (x) => Just (f (x)),
      () => Nothing (),
    ),
    Nothing: Nothing,
  }))
  | addMethod ('valueOf', () => ({
    Just: x => 'Just ' + JSON.stringify (x),
    Nothing: 'Nothing' | always,
  }))
