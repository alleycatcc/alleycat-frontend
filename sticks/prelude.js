import {
  pipe, compose, composeRight,
  recurry, dot1, dot2, dot,
} from 'stick-js/es'

export const fold = dot2 ('fold')
export const ap = dot1 ('ap')
export const apTo = dot1 ('apTo')
export const toString = dot ('toString')
export const valueOf = dot ('valueOf')

/* @todo move to stick-js */
export const flatMap = recurry (2) (
  f => xs => xs.flatMap ((x) => f (x)),
)
