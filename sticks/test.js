#!/usr/bin/env node

import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap, recurry, roll,
  map, filter, reject, reduce, flip, flip3,
  join, split, last, head, tail,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN,
  noop, blush, always, T, F,
  prop, path, has, hasIn,
  bindPropTo, bindProp, bindTo, bind,
  assoc, assocPath, assocM, assocPathM,
  concatTo, concat, appendTo, append,
  concatToM, concatM, appendToM, appendM,
  merge, mergeTo, mergeM, mergeToM,
  mergeIn, mergeInTo, mergeInM, mergeInToM,
  updateM, update, updatePathM, updatePath,
  lets, letS, compactOk, compact,
  die, raise, decorateException, exception,
  lt, gt, eq, ne, lte, gte,
  factory, factoryProps,
  toThe,
  againstEither,
  againstAny,
  arg0, plus, applyTo1,
  invoke,
} from 'stick-js/es'

import {
  cata, adt,
  is, dus,
  methodX,
  method, method1, method2,
  addMethod, addMethod1, addMethod2,
  isEitherOf, isAny,
  isAnyWith, isWith,
  isAnd, isAnyAnd,
} from './data'

import {
  Left, Right, fromLeft, fromRight,
} from './either'

import {
  Just, Nothing,
  fromJust,
} from './maybe'

import {
  fold,
  toString, valueOf,
  flatMap, ap, apTo,
} from './prelude'

import { curry, } from './manual'
const { PI: pi, } = Math
const { log, } = console

const logValue = valueOf >> log
// const logValue = toString >> log
const logWith = (header) => (...args) => log (... [header, ...args])
const logValueWith = valueOf >> logWith

const multiply = a => b => a * b
const double = 2 | multiply
const squared = toThe (2)
const inc = plus (1)
const foldDouble = fold (double)
const add3 = curry (function (a, b, c) { return a + b + c })

// :: Either a (Num -> Num)
const rd = double | Right
// :: Either (Num -> Num) a
const ld = double | Left
// :: Either a Num
const rn = 42 | Right
// :: Either String a
const ln = 'bad news' | Left

const just3 = 3 | Just
const nothing = Nothing ()

const foldEither1 = fold (
  l => 'left: ' + l,
  r => 'right: ' + r,
)

const foldEither2 = fold (
  l => 'left: ' + l,
) (
  r => 'right: ' + r,
)

const { Circle, Square, Rectangle, Squiggle, Shape, } = adt ('Shape', {
  Circle: ['radius'],
  Square: ['side'],
  Rectangle: ['side'],
  Squiggle: [],
})

// :: Shape
const circle = 1 | Circle
// :: Shape
const square = 6 | Square
// :: Shape
const rectangle = 10 | Rectangle
// :: Shape
const squiggle = Squiggle ()

// :: a -> [a](1)
const toSingletonList = x => [x]

// --- simple form, uses cata, no complicated predicates.
// :: Shape -> Num | String
const area1 = method (() => ({
  Circle: squared >> multiply (pi),
  Square: squared,
  Rectangle: squared,
  Squiggle: _ => 'squiggly',
}))

// --- extended form, can combine predicates, otherwise.
// :: Shape -> Num | String
const area = methodX ([
  Circle | is | dus (arg0 >> squared >> multiply (pi)),
  [Rectangle, Square] | isAny | dus (squared),
  otherwise | dus (_ => 'squiggly'),
])

/*
data Shape = Circle { radius :: Int }
           | Square { side :: Int }
           | Rectangle { side :: Int }
           | Squiggle

shapeInfo :: Shape -> String
shapeInfo (Circle x) | x > 10 = "big circle or square (dimension is " <> show x <> ")"
shapeInfo (Square x) | x > 10 = "big circle or square (dimension is " <> show x <> ")"
shapeInfo (Circle x) | x > 5 = "medium circle (radius is " <> show x <> ")"
shapeInfo c@(Circle x) | area c > 1 = "small circle (area is " <> show (area c) <> ")"
shapeInfo (Circle _) = "very small circle (area is " <> show (area c) <> ")"
shapeInfo _ = "default shape"
*/

const shapeInfo = methodX ([
  [Circle, Square] | isAnyWith ((x) => x > 10) | dus (
    (x) => 'big circle or square (dimension is ' + String (x) + ')'
  ),
  Circle | isWith ((x) => x > 5) | dus (
    (x) => x | sprintf1 ('medium circle (radius is %s)')
  ),
  Circle | isAnd (area >> tap (logWith ('area')) >> gt (1)) | dus (
    (x) => x | sprintf1 ('small circle (radius is %s)')
  ),
  Circle | is | dus (
    (x) => x | sprintf1 ('very small circle (radius is %s)')
  ),
  otherwise | dus (_ => 'default shape'),
])

const circleBig = Circle (20)
const circleMedium = Circle (8)
const circleSmall = Circle (5)
const squareBig = Square (20)
const squareSmall = Square (5)

; [circleBig, circleMedium, circleSmall, squareBig, squareSmall]
| map (shapeInfo)
| map (logValue)

; [circle, square, rectangle, squiggle]
| map (area1)
| map (logValue)

; [circle, square, rectangle, squiggle]
| map (area)
| map (logValue)

const sparkle = Object.create (circle)
sparkle | area | logValue

just3 | fromJust | logValue
just3 | logValue
nothing | logValue

lets (
  () => fold (double, _ => 42),
  (f) => {
    just3 | f | logValue
    nothing | f | logValue
  },
)

just3 | foldDouble (_ => 43) | logValue
nothing | foldDouble (_ => 43) | logValue

Just (inc) | apTo (just3) | logValue
Nothing () | apTo (just3) | logValue
Just (inc) | apTo (nothing) | logValue
Nothing () | apTo (nothing) | logValue

just3 | ap (Just (inc)) | logValue
just3 | ap (Nothing ()) | logValue
nothing | ap (Just (inc)) | logValue
nothing | ap (Nothing ()) | logValue

'ok' | logValue
true | logValue
10 | logValue

const r = invoke (() => {
  const l = 'bad news' | Left
  const r = 10 | Right

  l | logValue
  r | logValue
  l | map (double) | map (double) | logValue
  r | map (double) | map (double) | logValue
  r | flatMap (double) | logValue

  l | foldEither1 | logValue
  r | map (double) | foldEither1 | logValue
  r | map (double) | foldEither2 | logValue

  r.map (double).fold (
    l => 'lefty: ' + l,
    r => 'righty: ' + r,
  )

  r.map (double).fold (
    l => 'lefty: ' + l,
  ) (
    r => 'righty: ' + r,
  )

  return r
})

const s = Object.create (r)
s | map (double) | foldEither1 | logValue
s | map (double) | foldEither2 | logValue
s.map (double).fold (
  l => 'lefty: ' + l,
  r => 'righty: ' + r,
)
s.map (double).fold (
  l => 'lefty: ' + l,
) (
  r => 'righty: ' + r,
)

add3 (10, 20, 30) | logValue
add3 (10, 20) (30) | logValue
add3 (10) (20) (30) | logValue

; [1, 2, 3, 4] | flatMap (double >> toSingletonList) | logValue

// --- [[1, 2], [3, 4]] >>= map (*2)
; [[1, 2], [3, 4]] | flatMap (map (double)) | logValue

// --- ok, but illegal in Haskell: [1, 2, 3, 4] >>= (*2)
; [1, 2, 3, 4] | flatMap (double) | logValue

rn | ap (rd) | logValueWith ('ap right with right')
// --- after ap: Left double
rn | ap (ld) | fromLeft | applyTo1 (10) | logValue
ln | ap (rd) | logValue
// --- after ap: Left double
// This would actually be illegal in Haskell (the Lefts are of different types).
ln | ap (ld) | fromLeft | applyTo1 (10) | logValue

rd | apTo (rn) | logValue
ld | apTo (rn) | fromLeft | applyTo1 (10) | logValue
rd | apTo (ln) | logValue
ld | apTo (ln) | fromLeft | applyTo1 (10) | logValue

'3' | Left | toString | logValue
'3' | Left | valueOf | logValue
'3' | Left | String | logValue
'3' | Left | logValue
true | Right | toString | logValue
true | Right | valueOf | logValue

// --- test currying.
invoke (() => {
  const { Bill, Ted, BT, } = adt ('BT', {
    Bill: ['count'],
    Ted: ['count'],
  })

  const _method0 = () => ({
    Bill: (count) => Ted (count),
    Ted: (count) => Bill (count),
  })

  const _method1 = (x) => ({
    Bill: (count) => Ted (count + x),
    Ted: (count) => Bill (count + x),
  })

  const _method2 = (x, y) => ({
    Bill: (count) => Ted (count + x*y),
    Ted: (count) => Bill (count + x*y),
  })

  // --- no need to test addMethod{1,2,3} separately -- they're all curried the same.
  addMethod ('slamp0', _method0, BT)
  addMethod ('slamp1') (_method0, BT)
  addMethod ('slamp2') (_method0) (BT)
  addMethod ('slamp3', _method0) (BT)

  // @todo
  // BT | addMethodX ('showCount', id)

  BT | addMethod ('showCount', () => ({
    Bill: id,
    Ted: id,
  }))
  const showCount = dot ('showCount')

  const bill = Bill (10)
  const billSmall = Bill (5)
  const ted = Ted (15)

  console.log ('hi')
  bill.showCount() | log

  ted.slamp0 () | logValue
  bill.slamp1 () | logValue
  ted.slamp2 () | logValue
  bill.slamp3 () | logValue

  const slam1 = method (_method0)
  bill | slam1 | logValue

  method1 (_method1) (0.1) (bill) | logValue
  method1 (_method1) (0.1, bill) | logValue
  method1 (_method1, 0.1, bill) | logValue
  method1 (_method1, 0.1) (bill) | logValue

  method2 (_method2) (0.1) (2) (bill) | logValue

  const g = (count) => count > 5
  const h = showCount >> eq (10)
  // const f = (o) => isAnyWith (g) ([Bill]) (o)
  // const f = (o) => isAnyWith (g, [Bill]) (o)
  // const f = (o) => isAnyWith (g, [Bill], o)
  // const f = (o) => isAnyWith (g, [Bill]) (o)
  // const f = (o) => isWith (g, Bill, o)
  // const f = (o) => isWith (g, Bill) (o)
  // const f = (o) => isAnyAnd (h, [Bill]) (o)
  // const f = (o) => isAnyAnd (h) ([Bill]) (o)
  // const f = (o) => isAnd (h) (Bill) (o)
  const f = (o) => isAnd (h, Bill) (o)
  const checkBill = methodX ([
    f | dus (() => 'ja'),
    otherwise | dus (() => 'nee'),
  ])
  bill | checkBill | logValue
  billSmall | checkBill | logValue
})
