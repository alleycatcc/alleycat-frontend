#!/usr/bin/env node
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");
var _typeof = require("@babel/runtime/helpers/typeof");
var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));
var _fs = _interopRequireDefault(require("fs"));
var _es = require("stick-js/es");
var _fishLib = _interopRequireWildcard(require("fish-lib"));
var _nodeSourceWalk = _interopRequireDefault(require("node-source-walk"));
var _util = _interopRequireDefault(require("util"));
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != _typeof(e) && "function" != typeof e) return { "default": e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && {}.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n["default"] = e, t && t.set(e, n), n; }
(0, _fishLib.bulletSet)({
  type: 'star'
});
var inspect = function inspect(x) {
  return _util["default"].inspect(x, {
    depth: null,
    colors: process.stdout.isTTY
  });
};
var getFilenamesCmd = function getFilenamesCmd() {
  return (0, _fishLib.sysSpawn)('find', [__dirname + '/../../app', '-name', '*.js'], {
    sync: true,
    outSplit: true
  });
};
var getFilenames = (0, _es.composeRight)(getFilenamesCmd, (0, _es.prop)('out'));
var getSource = function getSource(path) {
  return _fs["default"].readFileSync(path).toString();
};
var underline = (0, _es.sprintf1)("\x1B[4m%s\x1B[24m");
var indent = function indent(n) {
  return function (s) {
    return (0, _es.pipe)((0, _es.pipe)((0, _es.pipe)(n, (0, _es.repeatV)('  ')), (0, _es.appendM)(s)), (0, _es.join)(''));
  };
};
var onEntry = (0, _es.composeRight)(_fishLib.green, _fishLib.info);
var onImport = (0, _es.composeRight)((0, _es.composeRight)(underline, indent(1)), _fishLib.log);
var onDefaultImport = function onDefaultImport(s) {
  return (0, _es.pipe)((0, _es.pipe)((0, _es.pipe)([(0, _es.pipe)('default', _fishLib.brightRed), s], (0, _es.sprintfN)('%s as %s')), indent(2)), _fishLib.log);
};
var onNamedImport = function onNamedImport(_ref) {
  var _ref2 = (0, _slicedToArray2["default"])(_ref, 2),
    local = _ref2[0],
    imported = _ref2[1];
  return (0, _es.pipe)((0, _es.pipe)(local, (0, _es.eq)(imported)), (0, _es.ifYes)(function () {
    return (0, _es.pipe)((0, _es.pipe)(imported, indent(2)), _fishLib.log);
  }, function () {
    return (0, _es.pipe)((0, _es.pipe)((0, _es.pipe)([imported, local], (0, _es.sprintfN)('%s as %s')), indent(2)), _fishLib.log);
  }));
};
var makeWalker = (0, _es.pipe)(_nodeSourceWalk["default"], _es.neu);
var walk = (0, _es.pipe)('walk', _es.dot2);
var walkSource = function walkSource(source) {
  return (0, _es.pipe)(makeWalker, walk(source, (0, _es.deconstruct2)(function (_ref3) {
    var source = _ref3.source,
      imported = _ref3.imported,
      type = _ref3.type,
      local = _ref3.local;
    return function (node) {
      (0, _es.pipe)(type, (0, _es.condS)([(0, _es.pipe)((0, _es.pipe)('ImportDeclaration', _es.eq), (0, _es.guard)(function () {
        return (0, _es.pipe)(node, (0, _es.deconstruct)(function (_ref4) {
          var value = _ref4.source.value;
          return (0, _es.pipe)(value, onImport);
        }));
      })), (0, _es.pipe)((0, _es.pipe)('ImportDefaultSpecifier', _es.eq), (0, _es.guard)(function () {
        return (0, _es.pipe)(node, (0, _es.deconstruct)(function (_ref5) {
          var name = _ref5.local.name;
          return (0, _es.pipe)(name, onDefaultImport);
        }));
      })), (0, _es.pipe)((0, _es.pipe)('ImportSpecifier', _es.eq), (0, _es.guard)(function () {
        return (0, _es.pipe)(node, (0, _es.deconstruct)(function (_ref6) {
          var local = _ref6.local,
            imported = _ref6.imported;
          return (0, _es.pipe)((0, _es.pipe)([local, imported], (0, _es.map)((0, _es.prop)('name'))), onNamedImport);
        }));
      }))]));
    };
  })));
};
var walkFile = function walkFile(entry) {
  return (0, _es.pipe)((0, _es.pipe)(entry, getSource), walkSource);
};
var go = function go(entries) {
  return (0, _es.pipe)(entries, (0, _es.map)(function (entry) {
    return (0, _es.pipe)((0, _es.pipe)(entry, (0, _es.tap)(onEntry)), walkFile);
  }));
};
go(getFilenames());