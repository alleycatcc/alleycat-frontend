#!/usr/bin/env node


import fs from 'fs'

import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap, recurry,
  map, filter, reject, reduce, flip, flip3,
  join, split, last, head, tail,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN,
  noop, blush, always, T, F,
  prop, path,
  lets, letS,
  die, eq, ne,
  factory, factoryProps,
  neu,
  deconstruct, deconstruct2,
  timesV, appendM, ifYes, repeatV,
} from 'stick-js/es'

import fishLib, {
  log, info, warn, error, green, yellow, magenta, brightRed, cyan, brightBlue,
  sysSpawn, sprintf, getopt, bulletSet,
} from 'fish-lib'

import walkerMod from 'node-source-walk'

import util from 'util'

bulletSet ({ type: 'star', })

const inspect = x => util.inspect (x, { depth: null, colors: process.stdout.isTTY, })

// --- dies
const getFilenamesCmd = () => sysSpawn (
  'find',
  [__dirname + '/../../app', '-name', '*.js'],
  {
    sync: true,
    outSplit: true,
  },
)

const getFilenames = getFilenamesCmd >> prop ('out')

const getSource = (path) => fs.readFileSync (path).toString ()

const underline = sprintf1 ('\u001b[4m%s\u001b[24m')

const indent = n => s => n | repeatV ('  ') | appendM (s) | join ('')

const onEntry = green >> info
const onImport = underline >> indent (1) >> log
const onDefaultImport = s => ['default' | brightRed, s] | sprintfN ('%s as %s') | indent (2) | log
const onNamedImport = ([local, imported]) => local | eq (imported) | ifYes (
  () => imported | indent (2) | log,
  () => [imported, local] | sprintfN ('%s as %s') | indent (2) | log,
)

const makeWalker = walkerMod | neu
const walk = 'walk' | dot2
const walkSource = (source) => makeWalker
  | walk (
    source,
    deconstruct2 (
      ({ source, imported, type, local, }) => node => {
        // return node | inspect | log
        type | condS ([
          'ImportDeclaration' | eq | guard (
            () => node | deconstruct (
              ({ source: { value, }}) => value | onImport,
            ),
          ),
          'ImportDefaultSpecifier' | eq | guard (
            () => node | deconstruct (
              ({ local: { name, }}) => name | onDefaultImport,
            ),
          ),
          'ImportSpecifier' | eq | guard (
            () => node | deconstruct (
              ({ local, imported, }) => [local, imported]
              | map (prop ('name'))
              | onNamedImport,
            ),
          )
        ])
      },
    ),
  )

const walkFile = (entry) => entry
  | getSource
  | walkSource

const go = (entries) => entries | map (
  (entry) => entry | tap (onEntry) | walkFile
)

go (getFilenames ())
